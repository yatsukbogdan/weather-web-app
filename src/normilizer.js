const cities = require('./city.list_2');
const fs = require('fs');

// fs.readFile('./city.list.json', file => console.log(file))
//
const citiesNormalized = {};
console.log(cities.length);
for (let i = 0; i < cities.length; i++) {
    citiesNormalized[cities[i].id] = cities[i];
}

fs.writeFile("output.json", JSON.stringify(citiesNormalized), 'utf8', function (err) {
    if (err) {
        console.log("An error occured while writing JSON Object to File.");
        return console.log(err);
    }

    console.log("JSON file has been saved.");
});
