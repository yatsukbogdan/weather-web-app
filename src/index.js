import React from 'react';
import ReactDOM from 'react-dom';
import MainScreen from './screens/main';
import API from "./api";

API.init();

ReactDOM.render(<MainScreen />, document.getElementById('root'));
