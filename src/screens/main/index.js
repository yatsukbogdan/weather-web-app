import React, { Component } from 'react';
import API from '../../api';
import Styled from './styled-components';
import CurrentWeather from "../../components/current-weather";
import cities from '../../city.list';
import {
    convert5Days3HoursForecastToDailyForecast,
    convertTemperatureFromKelvinTo,
} from "../../utils";
import DailyWeather from "../../components/daily-weather";
import WeeklyWeather from "../../components/weekly-weather";
import CitiesModal from "../../components/cities-modal";
import CitiesModalButton from "./cities-modal-button";
import AddCityListModal from "../../components/add-city-list-modal";

export default class MainScreen extends Component {
    constructor(props) {
        super(props);

        const appStateFromLocalStorage = localStorage.getItem('appState');
        if (!appStateFromLocalStorage) {
            this.state = {
                addCityListModalOpened: false,
                citiesModalOpened: false,
                cityIds: [],
                currentCityId: null,
                currentForecastByCityId: {},
                dailyForecastByCityId: {},
                hourlyForecastByCityId: {},
                temperatureUnit: 'C' // 'K', 'C', 'F'
            };
        } else {
            this.state = JSON.parse(appStateFromLocalStorage);
        }

        window.onbeforeunload = () => localStorage.setItem('appState', JSON.stringify(this.state));
    }

    getTemperatureInCurrentUnits = temp => convertTemperatureFromKelvinTo(temp, this.state.temperatureUnit);

    getFullForecastByCityId = id => {
        this.getDailyForecastByCityId(id);
        this.getCurrentForecastByCityId(id);
    };

    getDailyForecastByCityId = async id => {
        const { data } = await API.getDailyForecastByCityId(id);
        this.setState(prevState => ({
            dailyForecastByCityId: {
                ...prevState.dailyForecastByCityId,
                [id]: convert5Days3HoursForecastToDailyForecast(data),
            },
            hourlyForecastByCityId: {
                ...prevState.dailyForecastByCityId,
                [id]: data,
            },
        }));
    };

    getCurrentForecastByCityId = async id => {
        const { data } = await API.getCurrentForecastByCityId(id);
        this.setState(prevState => ({
            currentForecastByCityId: {
                ...prevState.currentForecastByCityId,
                [id]: data,
            },
        }));
    };

    openCitiesModal = () => this.setState({ citiesModalOpened: true });
    closeCitiesModal = () => this.setState({ citiesModalOpened: false });

    openAddCityListModal = () => this.setState({ addCityListModalOpened: true });

    addCityById = id => this.setState({
        cityIds: [...this.state.cityIds, id],
        addCityListModalOpened: false,
    }, () => this.getFullForecastByCityId(id));

    removeCityById = id => {
        const index = this.state.cityIds.findIndex(_id => _id === id);
        const newCityIds = [
            ...this.state.cityIds.slice(0, index),
            ...this.state.cityIds.slice(index + 1)
        ];
        if (newCityIds.length === 0) {
            this.setState({
                cityIds: newCityIds,
                currentCityId: null,
            });
        } else {
            this.setState({
                cityIds: newCityIds,
                currentCityId: newCityIds[0],
            });
        }
    };

    setCurrentCityId = id => this.setState({
        currentCityId: id,
        citiesModalOpened: false,
    }, () => this.getFullForecastByCityId(id));

    render() {
        console.log(this.state);
        const {
            currentCityId,
            currentForecastByCityId,
            dailyForecastByCityId,
            hourlyForecastByCityId,
        } = this.state;

        if (this.state.cityIds.length === 0) return (
            <AddCityListModal
                cityIds={this.state.cityIds}
                opened={true}
                addCityById={this.addCityById}
            />
        );

        if (this.state.currentCityId === null) return (
            <CitiesModal
                cityIds={this.state.cityIds}
                close={this.closeCitiesModal}
                currentForecastByCityId={currentForecastByCityId}
                currentTemperatureUnits={this.state.temperatureUnit}
                getTemperatureInCurrentUnits={this.getTemperatureInCurrentUnits}
                openAddCityModal={this.openAddCityListModal}
                opened={true}
                removeCityById={this.removeCityById}
                setCurrentCityId={this.setCurrentCityId}
                setCurrentTemperatureUnits={unit => this.setState({ temperatureUnit: unit })}
            />
        );

        if (!currentCityId ||
            !currentForecastByCityId[currentCityId] ||
            !dailyForecastByCityId[currentCityId] ||
            !hourlyForecastByCityId[currentCityId]
        ) return null;

        const currentForecast = currentForecastByCityId[currentCityId];
        const hourlyForecast = hourlyForecastByCityId[currentCityId].list.slice(0, 9);
        const dailyForecast = dailyForecastByCityId[currentCityId].list;
        const city = cities[currentCityId];

        return (
            <Styled.Container>
                <CurrentWeather
                    city={city.name}
                    description={currentForecast.weather[0].main}
                    temperature={this.getTemperatureInCurrentUnits(currentForecast.main.temp)}
                />
                <DailyWeather
                    getTemperatureInCurrentUnits={this.getTemperatureInCurrentUnits}
                    hourlyForecast={hourlyForecast}
                    maxTemperature={this.getTemperatureInCurrentUnits(currentForecast.main.temp_max)}
                    minTemperature={this.getTemperatureInCurrentUnits(currentForecast.main.temp_min)}
                />
                <WeeklyWeather
                    dailyForecast={dailyForecast}
                    getTemperatureInCurrentUnits={this.getTemperatureInCurrentUnits}
                />
                <CitiesModal
                    cityIds={this.state.cityIds}
                    close={this.closeCitiesModal}
                    currentForecastByCityId={currentForecastByCityId}
                    currentTemperatureUnits={this.state.temperatureUnit}
                    getTemperatureInCurrentUnits={this.getTemperatureInCurrentUnits}
                    openAddCityModal={this.openAddCityListModal}
                    opened={this.state.citiesModalOpened}
                    removeCityById={this.removeCityById}
                    setCurrentCityId={this.setCurrentCityId}
                    setCurrentTemperatureUnits={unit => this.setState({ temperatureUnit: unit })}
                />
                <AddCityListModal
                    cityIds={this.state.cityIds}
                    opened={this.state.addCityListModalOpened}
                    addCityById={this.addCityById}
                />
                <CitiesModalButton onClick={this.openCitiesModal} />
            </Styled.Container>
        );
    }
}
