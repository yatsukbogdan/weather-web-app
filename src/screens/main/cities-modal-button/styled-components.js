import styled from 'styled-components';

const Container = styled.div`
    align-items: center;
    background-color: red;
    display: flex;
    height: 30px;
    justify-content: center;
    margin: 20px;
    width: 100px;
    border-radius: 15px;
    cursor: pointer;
`;

export default {
    Container
}
