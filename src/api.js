import axios from 'axios';
import {
    OPEN_WEATHER_API_KEY,
    OPEN_WEATHER_API_URL,
} from "./config";

const getUrlWithApiKey = url => `${url}&appid=${OPEN_WEATHER_API_KEY}`;

export default class API {
    static init() {
        const api = axios.create({ baseURL: OPEN_WEATHER_API_URL });
        api.defaults.params = {};
        api.defaults.params.appid = OPEN_WEATHER_API_KEY;
        this.api = api;
    }

    static getCurrentForecastByCityId (id) {
        return this.api.get(getUrlWithApiKey(`weather?id=${id}`));
    }

    static getDailyForecastByCityId (id) {
        return this.api.get(getUrlWithApiKey(`forecast?id=${id}`));
    }
}
