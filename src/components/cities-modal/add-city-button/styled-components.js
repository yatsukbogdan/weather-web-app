import styled from 'styled-components';

const Container = styled.div`
    align-items: center;
    background-color: green;
    display: flex;
    height: 30px;
    border-radius: 15px;
    justify-content: center;
    margin: 20px;
    width: 100px;
    cursor: pointer;
`;

export default {
    Container
}
