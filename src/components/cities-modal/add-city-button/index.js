import React, { Component } from 'react';
import Styled from './styled-components';
import PropTypes from 'prop-types';
import StyledText from '../../../components/styled-components/text';

export default class AddCityButton extends Component {
    render() {
        return (
            <Styled.Container onClick={this.props.onClick}>
                <StyledText>Add new city</StyledText>
            </Styled.Container>
        );
    }
}

AddCityButton.propTypes = {
    onClick: PropTypes.func.isRequired,
};
