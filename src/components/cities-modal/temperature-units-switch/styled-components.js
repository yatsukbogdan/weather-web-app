import styled from 'styled-components';

const Container = styled.div`
    background-color: #55a3c5ee;
    border-radius: 15px;
    display: flex;
    height: 30px;
    overflow: hidden;
    margin: 20px;
    width: auto;
`;

const UnitContainer = styled.div`
    align-items: center;
    background-color: ${props => props.choosen ? 'red' : 'rgba(255, 0, 0, 0.5)'};
    cursor: pointer;
    display: flex;
    height: 30px;
    justify-content: center;
    width: 40px;
`;

export default {
    Container,
    UnitContainer,
}
