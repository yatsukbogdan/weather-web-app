import React, { Component } from 'react';
import Styled from './styled-components';
import PropTypes from 'prop-types';
import StyledText from '../../styled-components/text';

export default class TemperatureUnitsSwitch extends Component {
    render() {
        return (
            <Styled.Container>
                <Styled.UnitContainer
                    onClick={() => this.props.onUnitClick('C')}
                    choosen={this.props.currentTemperatureUnits === 'C'}
                >
                    <StyledText>C°</StyledText>
                </Styled.UnitContainer>
                <Styled.UnitContainer
                    onClick={() => this.props.onUnitClick('F')}
                    choosen={this.props.currentTemperatureUnits === 'F'}
                >
                    <StyledText>F°</StyledText>
                </Styled.UnitContainer>
            </Styled.Container>
        );
    }
}

TemperatureUnitsSwitch.propTypes = {
    currentTemperatureUnits: PropTypes.string.isRequired,
    onUnitClick: PropTypes.func.isRequired,
};
