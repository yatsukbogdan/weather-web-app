import React, { Component } from 'react';
import Styled from './styled-components';
import PropTypes from 'prop-types';
import CityList from "./city-list";
import CloseModalButton from "./close-modal-button";
import AddCityButton from "./add-city-button";
import TemperatureUnitsSwitch from "./temperature-units-switch";

export default class CitiesModal extends Component {
    render() {
        if (!this.props.opened) return null;

        return (
            <Styled.Container>
                <CityList
                    cityIds={this.props.cityIds}
                    currentForecastByCityId={this.props.currentForecastByCityId}
                    getTemperatureInCurrentUnits={this.props.getTemperatureInCurrentUnits}
                    removeCityById={this.props.removeCityById}
                    setCurrentCityId={this.props.setCurrentCityId}
                />
                <Styled.ElementsContainer>
                    <TemperatureUnitsSwitch
                        currentTemperatureUnits={this.props.currentTemperatureUnits}
                        onUnitClick={this.props.setCurrentTemperatureUnits}
                    />
                    <CloseModalButton onClick={this.props.close} />
                    <AddCityButton onClick={this.props.openAddCityModal} />
                </Styled.ElementsContainer>
            </Styled.Container>
        );
    }
}

CitiesModal.propTypes = {
    cityIds: PropTypes.array.isRequired,
    close: PropTypes.func.isRequired,
    currentForecastByCityId: PropTypes.object.isRequired,
    currentTemperatureUnits: PropTypes.string.isRequired,
    setCurrentTemperatureUnits: PropTypes.func.isRequired,
    getTemperatureInCurrentUnits: PropTypes.func.isRequired,
    openAddCityModal: PropTypes.func.isRequired,
    opened: PropTypes.bool.isRequired,
    removeCityById: PropTypes.func.isRequired,
    setCurrentCityId: PropTypes.func.isRequired,
};
