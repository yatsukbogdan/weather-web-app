import React, { Component } from 'react';
import Styled from './styled-components';
import PropTypes from 'prop-types';
import cities from '../../../city.list';
import Item from "./item";

export default class CityList extends Component {
    render() {
        const {
            currentForecastByCityId,
            getTemperatureInCurrentUnits,
            removeCityById,
            setCurrentCityId,
        } = this.props;

        return (
            <Styled.Container>
                {this.props.cityIds.map(cityId => currentForecastByCityId[cityId] && (
                    <Item
                        city={cities[cityId].name}
                        key={String(cityId)}
                        onRemoveButtonClick={() => removeCityById(cityId)}
                        onClick={() => setCurrentCityId(cityId)}
                        temperature={getTemperatureInCurrentUnits(currentForecastByCityId[cityId].main.temp)}
                    />
                ))}
            </Styled.Container>
        );
    }
}

CityList.propTypes = {
    cityIds: PropTypes.array.isRequired,
    currentForecastByCityId: PropTypes.object.isRequired,
    getTemperatureInCurrentUnits: PropTypes.func.isRequired,
    removeCityById: PropTypes.func.isRequired,
    setCurrentCityId: PropTypes.func.isRequired,
};
