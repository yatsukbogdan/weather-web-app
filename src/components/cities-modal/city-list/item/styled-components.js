import styled from 'styled-components';
import Text from '../../../styled-components/text';

const Container = styled.div`
    align-items: center;
    cursor: pointer;
    display: flex;
    height: 100px;
    position: relative;
    padding: 0 20px;
    border-bottom: 2px solid #477797;
`;

const CityText = styled(Text)`
    font-size: 20px;
`;

const TemperatureText = styled(Text)`
    margin-left: auto;
    font-size: 40px;
`;

const RemoveButton = styled.div`
    margin-left: 20px;
    align-items: center;
    background-color: red;
    border-radius: 15px;
    display: flex;
    height: 30px;
    justify-content: center;
    padding: 0 15px;
`;

const RemoveButtonText = styled(Text)`
    font-size: 14px;
`;

export default {
    RemoveButtonText,
    CityText,
    Container,
    RemoveButton,
    TemperatureText,
}
