import React, { Component } from 'react';
import Styled from './styled-components';
import PropTypes from 'prop-types';

export default class Item extends Component {
    onRemoveButtonClick = ev => {
        ev.stopPropagation();
        this.props.onRemoveButtonClick();
    };

    render() {
        return (
            <Styled.Container onClick={this.props.onClick}>
                <Styled.CityText>{this.props.city}</Styled.CityText>
                <Styled.TemperatureText>{this.props.temperature}°</Styled.TemperatureText>
                <Styled.RemoveButton onClick={this.onRemoveButtonClick}>
                    <Styled.RemoveButtonText>Remove</Styled.RemoveButtonText>
                </Styled.RemoveButton>
            </Styled.Container>
        );
    }
}

Item.propTypes = {
    onClick: PropTypes.func.isRequired,
    onRemoveButtonClick: PropTypes.func.isRequired,
    city: PropTypes.string.isRequired,
    temperature: PropTypes.number.isRequired,
};
