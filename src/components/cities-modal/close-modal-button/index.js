import React, { Component } from 'react';
import Styled from './styled-components';
import PropTypes from 'prop-types';
import StyledText from '../../../components/styled-components/text';

export default class CloseModalButton extends Component {
    render() {
        return (
            <Styled.Container onClick={this.props.onClick}>
                <StyledText>Close</StyledText>
            </Styled.Container>
        );
    }
}

CloseModalButton.propTypes = {
    onClick: PropTypes.func.isRequired,
};
