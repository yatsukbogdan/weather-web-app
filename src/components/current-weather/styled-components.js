import styled from 'styled-components';
import Text from '../styled-components/text';

const Container = styled.div`
    align-items: center;
    display: flex;
    height: 200px;
    justify-content: center;
`;

const DataContainer = styled.div`
    align-items: center;
    display: flex;
    flex-direction: column;
    height: 200px;
    justify-content: center;
    width: 200px;
`;

const CityLabel = styled(Text)`
    font-size: 26px;
`;

const DescriptionText = styled(Text)`
    font-size: 12px;
`;

const TemperatureText = styled(Text)`
    font-size: 50px;
`;

export default {
    CityLabel,
    Container,
    DataContainer,
    DescriptionText,
    TemperatureText,
}
