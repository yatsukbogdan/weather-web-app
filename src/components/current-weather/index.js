import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Styled from './styled-components';

export default class CurrentWeather extends Component {
    render() {
        return (
            <Styled.Container>
                <Styled.DataContainer>
                    <Styled.CityLabel>{this.props.city}</Styled.CityLabel>
                    <Styled.DescriptionText>{this.props.description}</Styled.DescriptionText>
                    <Styled.TemperatureText>{this.props.temperature}°</Styled.TemperatureText>
                </Styled.DataContainer>
            </Styled.Container>
        );
    }
}

CurrentWeather.propTypes = {
    city: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    temperature: PropTypes.number.isRequired,
};
