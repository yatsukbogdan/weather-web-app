import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Styled from './styled-components';
import Header from "./components/header";
import HourlyWeatherList from "./components/hourly-weather-list";

export default class DailyWeather extends Component {
    render() {
        return (
            <Styled.Container>
                <Header
                    maxTemperature={this.props.maxTemperature}
                    minTemperature={this.props.minTemperature}
                />
                <HourlyWeatherList
                    getTemperatureInCurrentUnits={this.props.getTemperatureInCurrentUnits}
                    hourlyForecast={this.props.hourlyForecast}
                />
            </Styled.Container>
        );
    }
}

DailyWeather.propTypes = {
    getTemperatureInCurrentUnits: PropTypes.func.isRequired,
    hourlyForecast: PropTypes.array.isRequired,
    maxTemperature: PropTypes.number.isRequired,
    minTemperature: PropTypes.number.isRequired,
};
