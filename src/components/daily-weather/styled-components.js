import styled from 'styled-components';
import Text from '../styled-components/text';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    border-bottom: 1px solid white;
`;

const CityLabel = styled(Text)`
    font-size: 26px;
`;

const DescriptionText = styled(Text)`
    font-size: 12px;
`;

const TemperatureText = styled(Text)`
    font-size: 50px;
`;

export default {
    CityLabel,
    Container,
    DescriptionText,
    TemperatureText,
}
