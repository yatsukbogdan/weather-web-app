import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Styled from './styled-components';
import Item from "./item";
import moment from 'moment';

export default class HourlyWeatherList extends Component {
    render() {
        return (
            <Styled.Container>
                {this.props.hourlyForecast.map((data, index) => (
                    <Item
                        key={String(index)}
                        isFirst={index === 0}
                        temperature={this.props.getTemperatureInCurrentUnits(data.main.temp)}
                        time={moment.utc(data.dt * 1000).format('hA')}
                        weather={data.weather[0].main}
                    />
                ))}
            </Styled.Container>
        );
    }
}

HourlyWeatherList.propTypes = {
    getTemperatureInCurrentUnits: PropTypes.func.isRequired,
    hourlyForecast: PropTypes.array.isRequired,
};
