import styled from 'styled-components';
import Text from '../../../../styled-components/text';

const Container = styled.div`
    align-items: center;
    display: flex;
    flex-direction: column;
    height: 100%;
    justify-content: center;
    width: 60px;
`;

const TimeText = styled(Text)`
    font-size: 16px;
    margin-bottom: 10px;
    font-weight: ${props => props.isFirst ? 500 : 300};
`;

const TemperatureText = styled(Text)`
    font-size: 22px;
    font-weight: ${props => props.isFirst ? 500 : 300};
`;

export default {
    Container,
    TemperatureText,
    TimeText,
}
