import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Styled from './styled-components';

export default class Item extends Component {
    render() {
        return (
            <Styled.Container>
                <Styled.TimeText isFirst={this.props.isFirst}>{this.props.time}</Styled.TimeText>
                <Styled.TimeText isFirst={this.props.isFirst}>{this.props.weather}</Styled.TimeText>
                <Styled.TemperatureText isFirst={this.props.isFirst}>{this.props.temperature}°</Styled.TemperatureText>
            </Styled.Container>
        );
    }
}

Item.propTypes = {
    isFirst: PropTypes.bool.isRequired,
    temperature: PropTypes.number.isRequired,
    time: PropTypes.string.isRequired,
    weather: PropTypes.string.isRequired,
};
