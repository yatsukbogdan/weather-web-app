import styled from 'styled-components';

const Container = styled.div`
    display: -webkit-box;
    height: 100px;
    overflow-x: scroll;
    padding: 0 10px;
`;

export default {
    Container,
}
