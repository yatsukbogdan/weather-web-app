import styled from 'styled-components';
import Text from '../../../styled-components/text';

const Container = styled.div`
    align-items: center;
    border-bottom: white solid 1px;
    display: flex;
    height: 50px;
    justify-content: space-between;
    padding: 0 20px;
`;

const AllTextContainer = styled.div`
    align-items: center;
    display: flex;
    flex: 1;
    justify-content: space-between;
`;

const WeekDayText = styled(Text)``;

const TodayText = styled(Text)`
    font-weight: 400;
    margin-left: 10px;
`;

const MaxTemperatureText = styled(Text)`
    margin-left: auto;
`;

const MinTemperatureText = styled(Text)`
    color: black;
    margin-left: 20px;
`;

export default {
    Container,
    AllTextContainer,
    MaxTemperatureText,
    MinTemperatureText,
    TodayText,
    WeekDayText,
}
