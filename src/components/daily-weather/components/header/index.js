import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Styled from './styled-components';
import moment from 'moment';

export default class Header extends Component {
    render() {
        return (
            <Styled.Container>
                <Styled.WeekDayText>{moment().format('dddd')}</Styled.WeekDayText>
                <Styled.TodayText>TODAY</Styled.TodayText>
                <Styled.MaxTemperatureText>{this.props.maxTemperature}</Styled.MaxTemperatureText>
                <Styled.MinTemperatureText>{this.props.minTemperature}</Styled.MinTemperatureText>
            </Styled.Container>
        );
    }
}

Header.propTypes = {
    maxTemperature: PropTypes.number.isRequired,
    minTemperature: PropTypes.number.isRequired,
};
