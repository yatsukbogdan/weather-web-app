import styled from 'styled-components';
import Text from '../../styled-components/text';

const Container = styled.div`
    align-items: center;
    display: flex;
    height: 40px;
    padding: 0 20px;
`;

const WeekdayText = styled(Text)`
    font-size: 20px;
    width: 200px;
`;

const WeatherText = styled(Text)`
    font-size: 20px;
`;

const MaxTemperatureText = styled(Text)`
    font-size: 20px;
    margin-left: auto;
`;

const MinTemperatureText = styled(Text)`
    margin-left: 10px;
    font-size: 20px;
    color: black;
`;

export default {
    Container,
    MaxTemperatureText,
    MinTemperatureText,
    WeatherText,
    WeekdayText,
}
