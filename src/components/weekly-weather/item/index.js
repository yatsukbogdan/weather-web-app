import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Styled from './styled-components';

export default class Item extends Component {
    render() {
        return (
            <Styled.Container>
                <Styled.WeekdayText>{this.props.weekday}</Styled.WeekdayText>
                <Styled.WeatherText>{this.props.weather}</Styled.WeatherText>
                <Styled.MaxTemperatureText>{this.props.maxTemperature}</Styled.MaxTemperatureText>
                <Styled.MinTemperatureText>{this.props.minTemperature}</Styled.MinTemperatureText>
            </Styled.Container>
        );
    }
}

Item.propTypes = {
    maxTemperature: PropTypes.number.isRequired,
    minTemperature: PropTypes.number.isRequired,
    weather: PropTypes.string.isRequired,
    weekday: PropTypes.string.isRequired,
};
