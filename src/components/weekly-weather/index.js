import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Styled from './styled-components';
import Item from './item';
import moment from 'moment';

export default class WeeklyWeather extends Component {
    render() {
        return (
            <Styled.Container>
                {this.props.dailyForecast.map((data, index) => (
                    <Item
                        key={String(index)}
                        weather={data.weather[0].main}
                        weekday={moment(data.dt_txt, 'YYYY-MM-DD').format('dddd')}
                        maxTemperature={this.props.getTemperatureInCurrentUnits(data.main.temp_max)}
                        minTemperature={this.props.getTemperatureInCurrentUnits(data.main.temp_min)}
                    />
                ))}
            </Styled.Container>
        );
    }
}

WeeklyWeather.propTypes = {
    dailyForecast: PropTypes.array.isRequired,
    getTemperatureInCurrentUnits: PropTypes.func.isRequired,
};
