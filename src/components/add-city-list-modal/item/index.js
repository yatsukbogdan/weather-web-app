import React, { Component } from 'react';
import Styled from './styled-components';
import PropTypes from 'prop-types';

export default class Item extends Component {
    render() {
        return (
            <Styled.Container onClick={this.props.onClick}>
                <Styled.CityText>{this.props.city}</Styled.CityText>
            </Styled.Container>
        );
    }
}

Item.propTypes = {
    onClick: PropTypes.func.isRequired,
    city: PropTypes.string.isRequired,
};
