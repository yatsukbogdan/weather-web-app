import styled from 'styled-components';
import Text from '../../styled-components/text';

const Container = styled.div`
    align-items: center;
    cursor: pointer;
    display: flex;
    height: 50px;
    position: relative;
    padding: 0 20px;
    background-color: #55a3c5ee;
    border-bottom: 2px solid #477797;
`;

const CityText = styled(Text)`
    font-size: 20px;
`;

export default {
    CityText,
    Container,
}
