import styled from 'styled-components';

const Container = styled.div`
    background-color: #55a3c5ee;
    height: 100vh;
    left: 0;
    position: absolute;
    top: 0;
    width: 100vw;
    z-index: 10;
`;

export default {
    Container
}
