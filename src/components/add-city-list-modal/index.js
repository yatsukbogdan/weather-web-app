import React, { Component } from 'react';
import Styled from './styled-components';
import PropTypes from 'prop-types';
import cities from '../../city.list';
import Item from "./item";

export default class AddCityListModal extends Component {
    state = {
        input: ''
    };

    onInputChange = ev => this.setState({ input: ev.target.value });

    get filteredCityIds() {
        const availableCities = Object.keys(cities).filter(cityId => !this.props.cityIds.includes(cityId));
        if (this.state.input === '') return availableCities.slice(0, 50);
        return availableCities.filter(cityId => cities[cityId].name.indexOf(this.state.input) !== - 1).slice(0, 50);
    }

    render() {
        if (!this.props.opened) return null;

        return (
            <Styled.Container>
                <input value={this.state.input} onChange={this.onInputChange}/>
                {this.filteredCityIds.map(cityId => (
                    <Item
                        city={cities[cityId].name}
                        key={String(cityId)}
                        onClick={() => this.props.addCityById(cityId)}
                    />
                ))}
            </Styled.Container>
        );
    }
}

AddCityListModal.propTypes = {
    opened: PropTypes.bool.isRequired,
    cityIds: PropTypes.array.isRequired,
    addCityById: PropTypes.func.isRequired,
};
