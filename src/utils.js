export const convertTemperatureFromKelvinTo = (value, unit) => {
    switch (unit) {
        case 'C': return Math.floor(value - 273.15);
        case 'F': return  Math.floor(9/5*(value - 273.15) + 32);
        case 'K':
        default:
            return  Math.floor(value);
    }
};

export const convert5Days3HoursForecastToDailyForecast = forecast => {
    const listByDate = {};

    for (let i = 0; i < forecast.list.length; i++) {
        const date = forecast.list[i].dt_txt.substr(0, 10);
        if (listByDate[date]) continue;
        listByDate[date] = {
            ...forecast.list[i],
            dt_txt: date,
        };
    }

    const listArray = Object.keys(listByDate).reduce((arr, key) => [...arr, listByDate[key]], []);

    return {
        ...forecast,
        list: listArray,
    }
};
